#include <iostream>
#include<istream>
#include <cmath>

class Vector
{
public:
	Vector()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	Vector(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}


	friend Vector operator+(const Vector& a, const Vector& b);

	friend std::ostream& operator <<(std::ostream& out, const Vector& v);

	friend Vector operator*(const Vector& a, const int& b);

	friend Vector operator-(const Vector& a, const Vector& b);

	friend  std::istream& operator >>(std::istream& in, Vector& v);

	float operator[](int index)
	{
		switch (index)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		default:
			std::cout << " index error";
			return 0;
			break;
		}
	}

private:
	float x;
	float y;
	float z;
};

Vector operator+(const Vector& a, const Vector& b)
{
	return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator*(const Vector& a, const int& b)
{
	return Vector(a.x * b, a.y * b, a.z * b);
}

Vector operator-(const Vector& a, const Vector& b)
{
	return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::ostream& operator <<(std::ostream& out, const Vector& v)
{
	out << v.x << " " << v.y << " " << v.z << " ";
	return out;
}

std::istream& operator >>(std::istream& in, Vector& v)
{
	in >> v.x >> v.y >> v.z;
	return in;
}


int main()
{
	setlocale(LC_ALL, "rus");

	Vector v1(3.1, 2.1, 5.1);
	Vector v2(1.3, 1.2, 1.5);
	int size = 2;
	Vector v0;

	float v6;

	Vector X;
	Vector Y;

	Vector v3 = v1 * size;// ��������� 
	Vector v4 = v1 + v2;// �������� 
	Vector v5 = v1 - v2;// ���������

	std::cout << v1 << "(1 ������)" << " " << '\n' << v2 << "(2 ������)" << '\n';

	std::cout << '\n' << v5 << "���������" << '\n';

	std::cout << v4 << "��������" << '\n';

	std::cout << v3 << "��������� 1 ������� �� 2" << '\n';

	std::cout << v1[2] << "�����" << '\n';

	std::cout << v1 << '\n';
	std::cin >> v1; 	
	std::cout << v1 << '\n';
	std::cout << Y << '\n';

}